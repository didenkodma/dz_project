/* 1. Сенс використання прототипного наслідування в тому, що розробникам не потрібно переписувати один і той же код багато разів. Створюється новий прототип або  береться існуючий (до нього можна додати необхідний функціонал) і на його основі за домогою someNameOfNewObject.prototype = Object.create(previousObject.prototype) створюється новий об'єкт, який має методи і властивості, що записані в батьківському прототипі.
 2. super() в конструкторі класу-нащадка потрібно використовувати для того, щоб викликати батьківський конструктор, що в свою чергу допоможе реалізувати механизм наслідування і позбавить програміста необхідності ще раз переписувати подібний код. Цю функцію треба викликати в самому початку функції-конструктора класу-нащадка.
*/

class Employee {

    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }

    set name(value) {
        this._name = value
    }

    get name() {
        return this._name
    }

    set age(value) {
        this._age = value
    }

    get age() {
        return `${this._age} years`
    }

    set salary(value) {
        this._salary = value
    }

    get salary() {
        return `${this._salary}$`
    }
}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang
    }

    set salary(value) {
        this._salary = value
    }

    get salary() {
        return `${this._salary*3}$`
    }
}

let programmer1 = new Programmer('Dmytro', 30, 1000, ['js', 'php', 'python'])
let programmer2 = new Programmer('July', 25, 800, ['js', 'python'])
let programmer3 = new Programmer('Petro', 17, 500, ['python'])

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);