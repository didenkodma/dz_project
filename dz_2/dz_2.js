/* 

	a) Отримуємо дані з віддаленого джерела. Наприклад у форматі JSON. try...catch використовуємо щоб перевірити чи отримали ми дані взагалі та якщо так, чи відповідають вони очікуваним, тобто таким, з якими наш скрипт корректно відпрацює.

	б) Відбувається заповнення форми на сайті, try...catch використовуємо для валідації данних.

	в) Авторизація на сайті, try...catch також використовуємо для валідації данних.

	Конструкція try...catch часто використовується у випадках коли мі очікуємо на можливі винятки і отримуємо дані ззовні.

*/

const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];

// Створіємо нову кастомну помилку
class NotEnoughInfoError extends Error {

	constructor(bookName, parameter) {
		super();
		this.name = "NotEnoughInfoError";
		this.message = `Can't add book "${bookName}" cause parameter "${parameter}" doesn't exist.`;
	}
}

// Створюємо класс BookElement, за допомогую якого будем рендерити елементи списку
class BookElement {

	constructor({ author, name, price }) {

		if (!author || !price) {
			let undefBookParam = "author";
			if (!price) undefBookParam = "price";
			throw new NotEnoughInfoError(name, undefBookParam);
		}

		this.author = author;
		this.name = name;
		this.price = price;
		this.li = document.createElement('li');

	}

	render(container) {
		this.li.innerText = `Книга "${this.name}" автора ${this.author} коштує ${this.price}грн.`;
		container.append(this.li);
	}
}

// Знаходимо контейнер і вкладаємо в нього елемент ul
const root = document.querySelector('#root');
const list = document.createElement('ul');
root.append(list);

// Пробігаємось по массиву і рендеремо елементи, якщо виникає помилка "NotEnoughInfoError" відправляємо в консоль, інші помилки "прокидаємо" далі.
books.forEach((element) => {
	try {
		new BookElement(element).render(list);
	} catch (err) {
		if (err.name === "NotEnoughInfoError") {
			console.error(err);
		} else {
			throw err;
		}
	}

});

