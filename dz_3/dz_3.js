/*

	Деструктуризація це дуже вдале і цікаве рішення, спрямоване на покращення читабельності коду JS. Сутність деуструктуризації у тому, щоб розбити складну структуру (великий багаторівневий массив, об'єкт з великою кількістю властивостей і т.д.) на більш прості елементи. Простий приклад (деструктуруємо массив arr, створюємо константи a, b, c, d, з якими набагато простіше працювати ніж з arr[0][0], arr[0][1], arr[1][0], arr[1][1]):

	const arr = [[1, 2], [3, 4]];
	const [[a, b], [c, d]] = arr;

	Ще одне цікаве прикладне використання деструктуризації - необхідно поміняти змінні місцями, (раніше для цього використовували третю змінну, наприклад temp):

	let a = 5;
	let b = 9;
	[a, b] = [b, a];

*/

// Завдання 1

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const [...clients] = new Set([...clients1, ...clients2]);

// Завдання 2

const characters = [
	{
		name: "Елена",
		lastName: "Гилберт",
		age: 17,
		gender: "woman",
		status: "human"
	},
	{
		name: "Кэролайн",
		lastName: "Форбс",
		age: 17,
		gender: "woman",
		status: "human"
	},
	{
		name: "Аларик",
		lastName: "Зальцман",
		age: 31,
		gender: "man",
		status: "human"
	},
	{
		name: "Дэймон",
		lastName: "Сальваторе",
		age: 156,
		gender: "man",
		status: "vampire"
	},
	{
		name: "Ребекка",
		lastName: "Майклсон",
		age: 1089,
		gender: "woman",
		status: "vempire"
	},
	{
		name: "Клаус",
		lastName: "Майклсон",
		age: 1093,
		gender: "man",
		status: "vampire"
	}
];

const charactersShortInfo = characters.map(e => {
	let { name, lastName, age } = e;
	return shortObj = { name, lastName, age };
});

// Завдання 3

const user1 = {
	name: "John",
	years: 30
};

const { name, years: age, isAdmin = false } = user1;

// Вивів на екран, міг просто вивести в консоль (не до кінця зрозумів куди виводити результат)
const body = document.querySelector('body');
const p1 = document.createElement('p');
const p2 = document.createElement('p');
const p3 = document.createElement('p');

p1.innerText = name;
p2.innerText = age;
p3.innerText = isAdmin;

body.append(p1, p2, p3);

// Завдання 4

const satoshi2020 = {
	name: 'Nick',
	surname: 'Sabo',
	age: 51,
	country: 'Japan',
	birth: '1979-08-21',
	location: {
		lat: 38.869422,
		lng: 139.876632
	}
}

const satoshi2019 = {
	name: 'Dorian',
	surname: 'Nakamoto',
	age: 44,
	hidden: true,
	country: 'USA',
	wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
	browser: 'Chrome'
}

const satoshi2018 = {
	name: 'Satoshi',
	surname: 'Nakamoto',
	technology: 'Bitcoin',
	country: 'Japan',
	browser: 'Tor',
	birth: '1975-04-05'
}

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

// Завдання 5

const books = [{
	name: 'Harry Potter',
	author: 'J.K. Rowling'
}, {
	name: 'Lord of the rings',
	author: 'J.R.R. Tolkien'
}, {
	name: 'The witcher',
	author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
	name: 'Game of thrones',
	author: 'George R. R. Martin'
}

const newBooks = [...books, bookToAdd];

// Завдання 6

const employee = {
	name: 'Vitalii',
	surname: 'Klichko'
}

const employeeExpanded = { ...employee, age: 45, salary: 15000 };

console.log(employeeExpanded);

//Завдання 7

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'


