// Класс Card малює картку
class Card {

	constructor({ userId, postId, title, text, fName, lName, email }) {
		this.title = title;
		this.text = text;
		this.fName = fName;
		this.lName = lName;
		this.email = email;
		this.postId = postId;
		this.userId = userId;
		this.urlPosts = 'https://ajax.test-danit.com/api/json/posts';
		this.urlPost = `${this.urlPosts}/${this.postId}`;
		this.cardContainer = document.createElement('div');
		this.cardUserInfo = document.createElement('p');
		this.cardUserName = document.createElement('span');
		this.cardUserEmail = document.createElement('span');
		this.cardTitle = document.createElement('h3');
		this.cardText = document.createElement('p');
		this.cardButtonDelete = document.createElement('button');
		this.cardButtonEdit = document.createElement('button');
	}

	drawElems(selector) {

		this.cardUserName.innerText = `${this.fName} ${this.lName}`;
		this.cardUserName.className = 'card__user-name';
		this.cardUserEmail.innerText = this.email;
		this.cardUserEmail.className = 'card__user-email';
		this.cardUserInfo.className = 'card__user-info';
		this.cardUserInfo.append(this.cardUserName, this.cardUserEmail);

		this.cardTitle.innerText = this.title;
		this.cardTitle.className = "card__title";
		this.cardText.innerText = this.text;
		this.cardText.className = "card__text";
		this.cardButtonDelete.innerHTML = '<i class="fa fa-solid fa-trash"></i>';
		this.cardButtonDelete.className = 'card__button-delete';
		this.cardButtonEdit.innerHTML = '<i class="fa fa-sharp fa-solid fa-edit"></i>';
		this.cardButtonEdit.className = 'card__button-edit';

		this.cardContainer.className = 'card';
		this.cardContainer.append(this.cardUserInfo, this.cardTitle, this.cardText, this.cardButtonDelete, this.cardButtonEdit);

		document.querySelector(selector).insertAdjacentElement('afterbegin', this.cardContainer);

	}
	// Функціонал кнопки delete
	deletePost() {

		fetch(this.urlPost, { method: 'DELETE' })
			.then(res => {

				if (res.ok) {
					this.cardContainer.remove();
				}
				else {
					throw new Error(`Bad request: ${res.status}`);
				}

			})
			.catch(e => console.error(e));
	}

	// Функціонал кнопки edit
	editPost() {

		const popup = new EditPopup(this.postId, this.userId, this.cardTitle, this.cardText);
		popup.render('.wrapper');

	}

	render(selector) {
		this.drawElems(selector);

		this.cardButtonDelete.addEventListener('click', () => {
			this.deletePost();
		});

		this.cardButtonEdit.addEventListener('click', () => {
			this.editPost();
		});

	}

}

// Класс PopUp створює popup
class PopUp {

	constructor() {
		this.urlPosts = 'https://ajax.test-danit.com/api/json/posts';
		this.popupWrapper = document.createElement('div');
		this.popupBack = document.createElement('div');
		this.popup = document.createElement('div');
		this.popupForm = document.createElement('form');
		this.popupInputTitleLabel = document.createElement('label');
		this.popupInputTitle = document.createElement('input');
		this.popupTextAreaBodyLabel = document.createElement('label');
		this.popupTextAreaBody = document.createElement('textarea');
		this.popupCloseButton = document.createElement('button');
		this.popupSubmitButton = document.createElement('button');
	}

	drawElems(selector) {

		this.popupInputTitleLabel.className = 'postForm__label';
		this.popupInputTitle.className = 'postForm__input';
		this.popupTextAreaBodyLabel.className = 'postForm__label';
		this.popupTextAreaBody.className = 'postForm__textarea';
		this.popupForm.className = 'postForm';
		this.popupWrapper.className = 'popup-wrapper';
		this.popupBack.className = 'popup-back'
		this.popup.className = 'popup';
		this.popupCloseButton.className = 'close-button';
		this.popupSubmitButton.className = 'postForm__submit-btn';

		this.popupInputTitleLabel.innerText = 'Введіть назву публікації:';
		this.popupTextAreaBodyLabel.innerText = 'Введіть текст публікації:';
		this.popupCloseButton.innerHTML = '<i class="fa fa-regular fa-close"></i>';
		this.popupSubmitButton.innerText = 'Publish';

		this.popupInputTitle.id = "post-title";
		this.popupTextAreaBody.id = "post-text";
		this.popupInputTitleLabel.setAttribute("for", "post-title");
		this.popupTextAreaBodyLabel.setAttribute("for", "post-text");
		this.popupSubmitButton.type = 'submit';

		this.popupInputTitle.required = " ";
		this.popupTextAreaBody.required = " ";

		this.popupForm.append(this.popupInputTitleLabel, this.popupInputTitle, this.popupTextAreaBodyLabel, this.popupTextAreaBody, this.popupSubmitButton);
		this.popup.append(this.popupForm, this.popupCloseButton);
		this.popupWrapper.append(this.popup, this.popupBack);
		document.querySelector(selector).append(this.popupWrapper);
	}


	render(selector) {

		this.drawElems(selector);

		this.popupCloseButton.addEventListener('click', () => {
			this.popupWrapper.remove();
		});

		this.popupBack.addEventListener('click', () => {
			this.popupWrapper.remove();
		});

		this.popupForm.addEventListener('submit', (e) => {
			e.preventDefault();

			this.popupWrapper.remove();

			fetchPostRequest(this.urlPosts, this.popupInputTitle.value, this.popupTextAreaBody.value)
				.then(data => {
					const { userId, id, title, body } = data;
					new Card({
						userId,
						postId: id,
						fName: 'Leanne',
						lName: 'Graham',
						email: 'Sincere@april.biz',
						title,
						text: body
					}).render('.wrapper');

				})
				.catch((e) => console.error(e));

		});
	}
}

// Класс EditPopup розширює клас PopUp і малює popup для редагування конкретної картки
class EditPopup extends PopUp {

	constructor(postId, userId, title, text) {
		super();
		this.postId = postId;
		this.userId = userId;
		this.title = title;
		this.text = text;
		this.popupForm = document.createElement('form');
	}

	drawElems(selector) {
		super.drawElems(selector);
		this.popupSubmitButton.innerText = 'Change post';
	}

	render(selector) {

		this.drawElems(selector);

		this.popupCloseButton.addEventListener('click', () => {
			this.popupWrapper.remove();
		});

		this.popupBack.addEventListener('click', () => {
			this.popupWrapper.remove();
		});

		this.popupForm.addEventListener('submit', (e) => {
			e.preventDefault();

			this.popupWrapper.remove();

			fetchPutRequest(this.urlPosts, this.postId, this.userId, this.popupInputTitle.value, this.popupTextAreaBody.value)
				.then(data => {
					const { title, body } = data;

					this.title.innerText = title;
					this.text.innerText = body;

				})
				.catch((e) => console.error(e));

		});
	}


}

const urlUsers = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';

// Function expression fetchGetRequest відправляє запит з методом GET по заданому url
const fetchGetRequest = (url) => fetch(url)
	.then(result => result.json())
	.catch(e => console.error(e));

// Function expression fetchPostRequest відправляє запит з методом POST по заданому url
const fetchPostRequest = (url, title, body) => fetch(url, {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: title,
		body: body
	})
})
	.then(result => result.json())
	.catch(e => console.error(e));

// Function expression fetchPutRequest відправляє запит з методом PUT по заданому url
const fetchPutRequest = (url, postId, userId, title, body) => fetch(`${url}/${postId}`, {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: userId,
		title: title,
		body: body
	})
})
	.then(res => res.json())
	.catch(e => console.error(e));

// Адаптер для юзерів
const respUserAdapter = ({ id, name, email }) => {
	return {
		id,
		email,
		fName: name.split(' ')[0],
		lName: name.split(' ')[1]
	};
};

// Адаптер для постів
const respPostAdapter = ({ id, userId, title, body }) => {
	return {
		postId: id,
		id: userId,
		title,
		text: body
	};
};

// Загальний адаптер для картки
const cardObjAdapter = ({ fName, lName, email }, { userId, postId, title, text }) => {
	return {
		userId,
		postId,
		fName,
		lName,
		email,
		title,
		text
	}
};

// Створює кнопку додавання нової публікації
const createButtonAdd = (selector) => {
	const buttonAdd = document.createElement('button');
	buttonAdd.className = 'button-add';
	buttonAdd.innerText = 'Add post';
	buttonAdd.addEventListener('click', () => {
		const popup = new PopUp();
		popup.render(selector);
	});
	document.querySelector(selector).append(buttonAdd);
};

const removeElement = (selector) => {
	document.querySelector(selector).remove();
};

// Функція стартує всю логіку
const start = () => {

	Promise.allSettled([fetchGetRequest(urlUsers), fetchGetRequest(urlPosts)])
		.then(res => {

			const posts = res[1].value;
			const users = res[0].value;

			removeElement('.loader');
			createButtonAdd('.wrapper');

			posts.forEach(post => {

				const postAdapted = respPostAdapter(post);
				const [user] = users.filter((u) => u.id === postAdapted.id);
				const userAdapted = respUserAdapter(user);
				const cardObj = cardObjAdapter(userAdapted, postAdapted);
				new Card(cardObj).render('.wrapper');

			});

		})
		.catch(e => console.error(e));

};

start();








