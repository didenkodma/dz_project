import getAddress from "./getAddress.js";
import Card from "../classes/card.js";
import cardAdapter from "./cardAdapter.js";

function renderData() {
    const btn = document.querySelector('#find');
    const loader = document.querySelector('#loader');

    btn.addEventListener('click', () => {
        btn.classList.add('invisible');
        loader.classList.add('visible');

        (async () => {
            const data = await getAddress();
            const {query, continent, country, regionName, city, district} = await data;
            loader.classList.add('invisible');
            new Card(cardAdapter({query, continent, country, regionName, city, district})).render('.container');
        })();

    });
}

export default renderData;