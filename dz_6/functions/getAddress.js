import getIP from "./getIp.js";

async function getAddress() {
    const ipResponse = await getIP();
    const IP = await ipResponse.ip;
    const addressResponse = await fetch(`http://ip-api.com/json/${IP}?fields=continent,country,regionName,city,district,query`);
    const addressData = await addressResponse.json();
    return addressData;
}

export default getAddress;