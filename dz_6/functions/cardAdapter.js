function cardAdapter({query, continent, country, regionName, city, district}) {
    return {
        ip: query,
        continent, 
        country,
        region: regionName, 
        city, 
        district}
}

export default cardAdapter;