async function getIP() {
    const data = await fetch('https://api.ipify.org/?format=json');
    const dataDecoded = await data.json();
    return dataDecoded;
}   

export default getIP;