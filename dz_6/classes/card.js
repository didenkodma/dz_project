class Card {

    constructor({ip, continent, country, region, city, district}) {
        this.ip = ip;
        this.continent = continent ? continent : 'не знайдено';;
        this.country = country ? country : 'не знайдено';
        this.region = region ? region : 'не знайдено';
        this.city = city ? city : 'не знайдено';
        this.district = district ? district : 'не знайдено';
    }

    render(selector) {

        document.querySelector(selector).insertAdjacentHTML('beforeend', `
            <div class="card">
            <p class="card__info">Інформуємо:</p>
            <p class="card__ip">Ваша IP-адреса: ${this.ip}</p>
            <p class="card__continent">Ваш континент: ${this.continent}</p>
            <p class="card__country">Ваша країна: ${this.country}</p>
            <p class="card__region">Ваш регіон: ${this.region}</p>
            <p class="card__city">Ваше місто: ${this.city}</p>
            <p class="card__district">Ваш район: ${this.district}</p>
            </div>
        `);
    }

}

export default Card;