/*

AJAX - це підхід, що дозволяє підвантажувати дані з сервера, не перезавантажуючи сторінку. Цей підхід реалізує  client-side rendering. Тобто ситуацію, коли рендер сторінки або її частини відбувається на боці клієнта. 

Реалізується все це за допомогою Fetch або XMLHttpRequest. Дані, що пересилаються, не прив'язані в AJAX до формату XML, просто назва підходу вигадана досить давно.

*/

//Практичне завдання

const url = 'https://ajax.test-danit.com/api/swapi/films';
const filmsContainer = document.querySelector('.films-container');

fetch(url)
	.then(response => response.json())
	.then(films => {

		films.forEach(film => {
			const { characters, episodeId, name, openingCrawl } = film;
			const filmContainer = document.createElement('div');
			const filmEpisodeNum = document.createElement('p');
			const filmName = document.createElement('p');
			const filmOpeningCrawl = document.createElement('p');
			const filmCharactersList = document.createElement('ul');

			filmContainer.className = 'film';

			filmEpisodeNum.innerText = `Episode: ${episodeId}`;
			filmName.innerText = name;
			filmCharactersList.innerHTML = "<li class='film__list-preloader'><span class='color'></span></li>";

			filmOpeningCrawl.innerText = openingCrawl;
			filmContainer.append(filmEpisodeNum, filmName, filmCharactersList, filmOpeningCrawl);
			filmsContainer.append(filmContainer);

			const charactersNamesArr = characters.map((character) => fetch(character).then(res => res.json()));

			Promise.allSettled(charactersNamesArr)
				.then((res) => {
					filmCharactersList.innerHTML = "";
					res.forEach(r => {
						const {name} = r.value;
						filmCharactersList.innerHTML += `<li class='film__list-character'>${name}</li>`;
					});
				});

		});
	})
	.catch(e => console.error(e));



